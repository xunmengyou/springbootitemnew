package com.wwy.bootitem;


import com.wwy.controller.TestStarterController;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

public class TestStarterControllerTest extends BaseTest {
    @Autowired
    TestStarterController testStarterController;

//    @Mock
//    TestStarterController testStarterController1;
//
//    @InjectMocks
//    TestStarterController testStarterController2;

    /**
     * @Test：声明需要测试的方法
     * @BeforeClass：针对所有测试，只执行一次，且必须为static void；
     * @AfterClass：针对所有测试，只执行一次，且必须为static void；
     * @Before：每个测试方法前都会执行的方法；
     * @After：每个测试方法前都会执行的方法；
     * @Ignore：忽略方法；
     */
    @Test
    public void hbaseOnline1(){
       Object ob = testStarterController.hbaseOnline();
      System.out.println(ob);
    }


//    @Test
//    public void hbaseOnline2(){
////        Object ob = testStarterController1.hbaseOnline();
//
//        Object ob2 = testStarterController2.hbaseOnline();
//        System.out.println(ob2);
//    }


    /**
     * 断言测试
     *
     * Assert.assertEquals 对比两个值相等
     * Assert.assertNotEquals 对比两个值不相等
     * Assert.assertSame 对比两个对象的引用相等
     * Assert.assertArrayEquals 对比两个数组相等
     * Assert.assertTrue 验证返回是否为真
     * Assert.assertFlase 验证返回是否为假
     * Assert.assertNull 验证null
     * Assert.assertNotNull 验证非null
     */

}
