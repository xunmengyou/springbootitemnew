package com.wwy.bootitem;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * AbstractTransactionalJUnit4SpringContextTests可以在操作数据库后，将事务回滚
 *
 * @RunWith：标识为JUnit的运行环境
 *@SpringBootTest：获取启动类、加载配置，确定装载Spring Boot
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class BaseTest extends AbstractTransactionalJUnit4SpringContextTests {

}
