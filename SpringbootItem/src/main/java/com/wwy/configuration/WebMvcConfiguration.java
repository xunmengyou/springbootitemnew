package com.wwy.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 重写WebMvcConfigurerAdapter的方法来添加自定义拦截器，消息转换器等
 */
//@Configuration
//@RefreshScope
public class WebMvcConfiguration implements WebMvcConfigurer {

    private static final Logger LOG = LoggerFactory.getLogger(WebMvcConfiguration.class);
//
//    @Value("${cors.allowedOrigins:'.com.com'}")
//    private String[] allowedOrigins;
//
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private Environment env;
//
//    /**
//     * 设置跨域请求
//     *
//     * @param registry
//     */
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        // 对跨域生效的路径
//        registry.addMapping("/**")
//                // 支持跨域的域名
//                .allowedOrigins(allowedOrigins)
//                // 允许跨域的请求头
//                .allowedHeaders("*")
//                // 允许跨域的请求方法
//                .allowedMethods("*")
//                .allowCredentials(true)
//                .maxAge(3600);
//    }
//
//    /**
//     * 注册拦截器
//     */
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new UserAuthInterceptor(userService, env))
//                .addPathPatterns("/**/private/**/*");
//    }
//
//    @Bean
//    public ServletContextInitializer initializer() {
//        return servletContext -> LOG.info("path : {}", servletContext.getContextPath());
//    }

}
