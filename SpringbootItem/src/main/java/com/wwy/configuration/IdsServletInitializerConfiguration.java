package com.wwy.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 18070510
 */
@Configuration
public class IdsServletInitializerConfiguration {
    /**
     * 添加鉴权过滤器
     */
//    @Bean
//    @ConditionalOnClass(AuthenticationFilter.class)
//    public FilterRegistrationBean addAuthenticationFilter() {
//        FilterRegistrationBean<AuthenticationFilter> filterRegistrationBean = new FilterRegistrationBean<>();
//        filterRegistrationBean.setFilter(new AuthenticationFilter());
//        filterRegistrationBean.addInitParameter("authUrlPattern", "/auth");
//        filterRegistrationBean.addInitParameter("authStatusUrlPattern", "/authStatus");
//        filterRegistrationBean.addInitParameter("popupLoginSuccessUrlPattern", "/popupLoginSuccess");
//        filterRegistrationBean.addUrlPatterns("/*");
//        return filterRegistrationBean;
//    }
//
//    @Bean
//    @ConditionalOnClass(AuthenticationServlet.class)
//    public ServletRegistrationBean initializeAuthenticationServlet() {
//        ServletRegistrationBean registrationBean = new ServletRegistrationBean<>(new AuthenticationServlet(), "/auth");
//        registrationBean.setLoadOnStartup(3);
//        return registrationBean;
//    }
//
//    @Bean
//    @ConditionalOnClass(AuthStatusServlet.class)
//    public ServletRegistrationBean initializeAuthStatusServlet() {
//        ServletRegistrationBean registrationBean = new ServletRegistrationBean<>(new AuthStatusServlet(), "/authStatus");
//        registrationBean.setLoadOnStartup(3);
//        return registrationBean;
//    }
//
//    @Bean
//    @ConditionalOnClass(PopupLoginSuccessServlet.class)
//    public ServletRegistrationBean initializePopupLoginSuccessServlet() {
//        ServletRegistrationBean registrationBean = new ServletRegistrationBean<>(new PopupLoginSuccessServlet(), "/popupLoginSuccess");
//        registrationBean.setLoadOnStartup(3);
//        return registrationBean;
//    }
}
