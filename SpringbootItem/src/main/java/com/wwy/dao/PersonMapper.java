package com.wwy.dao;

import com.wwy.entity.Person;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonMapper {
    public void insertPerson(Person person);
    public Person selectPersonById(Integer id);
    public Integer delPerson(Person person);
}
