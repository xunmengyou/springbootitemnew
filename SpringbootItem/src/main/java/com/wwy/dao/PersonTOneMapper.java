package com.wwy.dao;

import com.wwy.entity.PersonT1;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonTOneMapper {
    public void insertPerson1(PersonT1 pers1);
    public PersonT1 selectPerson1ById(Integer id);
    public Integer delPerson1(PersonT1 pers1);
}
