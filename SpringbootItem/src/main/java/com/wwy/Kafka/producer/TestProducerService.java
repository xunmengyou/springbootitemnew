package com.wwy.Kafka.producer;

import com.wwy.Kafka.KafkaProducerUtil;
import com.wwy.Kafka.TestProducerProperties;
import kafka.javaapi.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.stereotype.Service;

/**
 * 活动商品kafka 生产者
 */
@Service
public class TestProducerService extends KafkaProducerUtil {

    private static volatile Producer<String,String> producer;

    private static volatile KafkaProducer<String,String> kafkaProducer;

    @Override
    public String getTopic() {
        return TestProducerProperties.TOPIC;
    }

    @Override
    protected String getBrokerList() {
        return TestProducerProperties.BROKER;
    }

    @Override
    protected String getPartitioner() {
        return "com.suning.mbap.kafka.HashPartitioner";
    }

    @Override
    protected int getNumPartitions() {
        return Integer.valueOf(TestProducerProperties.NUMPARTITIONS);
    }

    @Override
    protected int getRetries() {
        return 0; //默认传值，可以配置
    }

    @Override
    protected int getRetriesBackoff() {
        return 0;  //默认传值，可以配置
    }

    @Override
    protected Producer<String, String> getProducer() {
        if(null == producer){
            synchronized (TestProducerService.class){
                if(null == producer){
                    producer = new Producer<String, String>(getProducerConfig());
                }
            }
        }
        return producer;
    }

    @Override
    protected KafkaProducer<String, String> getKafkaProducer() {
        if(null == kafkaProducer){
            synchronized (TestProducerService.class){
                if(null == kafkaProducer){
                    kafkaProducer = new KafkaProducer<String,String>(getPropertiesV2());
                }
            }
        }
        return kafkaProducer;
    }
}
