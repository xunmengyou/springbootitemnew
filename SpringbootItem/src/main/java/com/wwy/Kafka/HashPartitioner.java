package com.wwy.Kafka;

import kafka.producer.Partitioner;
import kafka.utils.VerifiableProperties;

import java.util.Random;

/**
 * 自定义分区器
 */
public class HashPartitioner implements Partitioner {
    public HashPartitioner(VerifiableProperties props) {
    }

    @Override
    public int partition(Object o, int partitions) {
        if (null == o) {
            Random random = new Random();
            return random.nextInt(partitions);
        } else {
            int result = Math.abs(o.hashCode()) % partitions;
            return result;
        }
    }
}
