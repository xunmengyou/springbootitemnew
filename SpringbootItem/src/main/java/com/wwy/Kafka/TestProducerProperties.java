package com.wwy.Kafka;

import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Properties;

@Service
public class TestProducerProperties {

    private static Properties prop = new Properties();

    static {
        InputStream inStream = TestProducerProperties.class.getClassLoader().getResourceAsStream("application.properties");
        try {
            prop.load(inStream);
        } catch (Exception e) {

        }
    }

    public static final String BROKER = prop.getProperty("avtivityProduct_broker");

    public static final String TOPIC = prop.getProperty("avtivityProduct_topic");

    public static final String NUMPARTITIONS = prop.getProperty("avtivityProduct_numPartitions");
}
