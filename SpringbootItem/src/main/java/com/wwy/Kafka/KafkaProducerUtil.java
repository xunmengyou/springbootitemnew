package com.wwy.Kafka;
import com.alibaba.fastjson.JSON;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

/**
 * kafka生产者公共抽象类
 */
public abstract class KafkaProducerUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerUtil.class);
    //保证不同对象获得值不一样
    public Random RANDOM = new Random();

    /**
     * 获取topic
     * @return
     */
    public abstract String getTopic();
    protected abstract String getBrokerList();
    protected abstract String getPartitioner();
    protected abstract int getNumPartitions();

    protected  abstract int getRetries();
    protected  abstract int getRetriesBackoff();

    protected abstract Producer<String, String> getProducer();
    protected abstract KafkaProducer<String, String> getKafkaProducer();

    protected Properties getProperties(){
        Properties properties = new Properties();
        // 此处配置的是kafka的端口
        properties.put("metadata.broker.list", getBrokerList());
        // 定义准备传递数据给broker时使用哪个序列化器。
        properties.put("serializer.class", "kafka.serializer.StringEncoder");
        // 这个是可选项，该类将决定消息将发送到哪个主题分区上。
        properties.put("partitioner.class", getPartitioner());
        // 异步，超时时间
        properties.put("producer.type", "async");
        properties.put("request.timeout.ms", "3000");
        /**
         * 消息确认机制   值为0,1,-1,可以参考
         *  1：表示只要Partition Leader接收到消息而且写入本地磁盘了，就认为成功了，不管他其他的Follower有
         *    没有同步过去这条消息了。
         */
        properties.put("request.required.acks", "1");
        /**
         * retries和retries.backoff.ms决定了重试机制，也就是如果一个请求失败了可以重试几次，每次重试的间隔是多少毫秒
         */
        properties.put("retries", getRetries()); //0
        properties.put("retries.backoff.ms", getRetriesBackoff()); //20
        return properties;
    }

    protected Map<String,Object> getPropertiesV2(){
        Map<String,Object> properties = new HashMap<>();
        // 此处配置的是kafka的端口
        properties.put("metadata.broker.list", getBrokerList());
        // 定义准备传递数据给broker时使用哪个序列化器。
        properties.put("serializer.class", "kafka.serializer.StringEncoder");
        // 这个是可选项，该类将决定消息将发送到哪个主题分区上。
        properties.put("partitioner.class", getPartitioner());
        properties.put("request.required.acks", "1");
        properties.put("retries", getRetries()); //0
        properties.put("retries.backoff.ms", getRetriesBackoff()); //20
        // 异步，超时时间
        properties.put("producer.type", "async");
        properties.put("request.timeout.ms", "3000");
        properties.put("buffer.memory",33554432);//32M,发送消息的缓冲区
        properties.put("compression.type","lz4");
        properties.put("batch.size",32768); //32KB，batch的大小
        properties.put("linger.ms",100); //默认0，此时batch立马发出。100表示batch在未满下至少等待100ms才能发出去，如果满了不需要等待延迟
        return properties;
    }

    protected ProducerConfig getProducerConfig() {
        ProducerConfig config = new ProducerConfig(getProperties());
        return config;
    }

    /**
     * 老的发送方式是：发送并忘记方式
     * @param obj
     */
    public synchronized void send(Object obj) {
        // 发送对象转JSON
        String message = JSON.toJSONString(obj);
        // 随机发送到各个节点分区
        String key = String.valueOf(RANDOM.nextInt(getNumPartitions()));
        // 发送消息到消息中介，kafka_topic指定要接受消息的主题
        KeyedMessage<String, String> data = new KeyedMessage<String, String>(getTopic(), key, message);
        try {
            //执行发送
            getProducer().send(data);
        } catch (Exception e) {
            LOGGER.error("kafka send  topic:"+getTopic()+" exception :"+e);
        }
    }

    /**
     * 同步发送
     * @param obj
     */
    public synchronized RecordMetadata syncSend(Object obj) {
        // 发送对象转JSON
        String message = JSON.toJSONString(obj);
        // 随机发送到各个节点分区
        String key = String.valueOf(RANDOM.nextInt(getNumPartitions()));
        //执行发送
        ProducerRecord<String,String> data = new ProducerRecord<String,String>(getTopic(),
                key,message);

        RecordMetadata metadata = null;
        try {
            metadata = getKafkaProducer().send(data).get();
         //  System.out.println("同步发送后获得分区为 :" + metadata.partition() + " ，同步发送后获得offset为 :" + metadata.offset());
        } catch (Exception e) {
            LOGGER.error("kafka syncSend topic:"+getTopic()+"  message :"+message+ " exception :" +e);
        }
        return metadata;
    }

    /**
     * 异步发送+回调
     * @param obj
     */
    public synchronized void asyncCallbackSend(Object obj) {
        // 发送对象转JSON
        String message = JSON.toJSONString(obj);
        // 随机发送到各个节点分区
        String key = String.valueOf(RANDOM.nextInt(getNumPartitions()));
        //执行发送
        ProducerRecord<String,String> data = new ProducerRecord<String,String>(getTopic(),
                key,message);
        try {
            getKafkaProducer().send(data, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (recordMetadata != null) {
                        //异步发送，成功
                 //       System.out.println("异步发送后获得分区为 :" + recordMetadata.partition() + " ，同步发送后获得offset为 :" + recordMetadata.offset());
                    }else{
                        //异步发送，失败
                    }
                }
            });
        } catch (Exception e) {
            LOGGER.error("kafka asyncCallbackSend topic:"+getTopic()+" message :"+message+ " exception :" +e);
        }
    }
}
