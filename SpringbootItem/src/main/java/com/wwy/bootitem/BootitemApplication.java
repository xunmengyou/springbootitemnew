package com.wwy.bootitem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 不加@SpringBootApplication注解，Unable to start ServletWebServerApplicationContext due to missing ServletWebServerFactory bean.
 * ------------因为没有 ServletWebServerFactory，而导致无法启动IOC容器
 * (scanBasePackages= {"com.wwy"})是由于当前启动类和其他类不在同一个包路径下，所以加scan的配置
 */
@SpringBootApplication(scanBasePackages= {"com.wwy"})
@EnableAspectJAutoProxy
@MapperScan(basePackages = {"com.wwy.*"}) //可以测试各个层
public class BootitemApplication {

	public static void main(String[] args) {
		/**
		 * ConfigurableApplicationContext是ApplicationContext的子类
		 * run启动就是spring的初始化操作 （IOC）
		 */
		ConfigurableApplicationContext appcontext = SpringApplication.run(BootitemApplication.class, args);
	}

}
