package com.wwy.bootitem;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

/**
 * Jedis集成不太行
 */
//@Configuration
public class RedisJedisConfig {

    @Value("${spring.redis.cluster.nodes}")
    private String nodes;


    @Bean
    public JedisCluster getJedisCluster(){
        String[] node = nodes.split(",");
        Set<HostAndPort> nodeSet = new HashSet<>();

        for(String ipPort : node){
            String[] te = ipPort.split(":");
            nodeSet.add(new HostAndPort(te[0],Integer.valueOf(te[1])));
        }
        return new JedisCluster(nodeSet,10000);
    }

}
