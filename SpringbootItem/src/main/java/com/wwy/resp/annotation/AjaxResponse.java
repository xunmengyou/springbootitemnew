package com.wwy.resp.annotation;

import java.util.HashMap;
import java.util.List;

/**
 * {ret:"0", msg:"", data:{}}
 */
public class AjaxResponse extends HashMap<String,Object>{
    public static final String RET_KEY = "ret";
    public static final String MSG_KEY = "msg";
    public static final String DATA_KEY = "data";
    public static final String RET_OK = "0";
    public static final String RET_UNCATCHED = "-9"; //服务器异常
    public static final String MSG_UNCATCHED = "服务器异常";
    public static AjaxResponse success() {
        return json(RET_OK);
    }

    public static AjaxResponse success(List<String> key, List<Object> value) {
        return json(key, value, RET_OK);
    }

    public static AjaxResponse error(String code, String msg) {
        return json(MSG_KEY, msg, code);
    }

    public AjaxResponse json(String key, Object value) {
        this.put(key, value);
        return this;
    }

    public AjaxResponse jsonData(Object value) {
        this.put(DATA_KEY, value);
        return this;
    }

    public AjaxResponse json(List<String> keys, List<Object> values) {
        if (keys == null) {
            return this;
        }
        for (int i = 0; i < keys.size(); i++) {
            this.put(keys.get(i), values.get(i));
        }
        return this;
    }

    public static AjaxResponse json() {
        return new AjaxResponse();
    }

    public static AjaxResponse json(String code) {
        return json().json(RET_KEY, code);
    }

    public static AjaxResponse json(String key, Object value, String code) {
        return json(code).json(key, value);
    }

    public static AjaxResponse json(List<String> keys, List<Object> values, String code) {
        return json(code).json(keys, values);
    }
}
