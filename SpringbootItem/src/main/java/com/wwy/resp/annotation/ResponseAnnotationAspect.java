package com.wwy.resp.annotation;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Method;

@Aspect  //是把当前类标识为一个切面供容器读取
@Component
public class ResponseAnnotationAspect {

    //切入点，描述信息，修饰JoinPoint
    @Pointcut("@annotation( com.wwy.resp.annotation.ResponseAnnotation)")
    public void backPointCut() {}

    @Around(value="backPointCut()")
    public Object aroundAuthority(ProceedingJoinPoint proceedJp) throws Throwable{
        // 寻找包含Wrapper注解的方法，找不到则不继续执行aop逻辑
        //getSignature() 获取封装了署名信息的对象,在该对象中可以获取到目标方法名,所属类的Class等信息
        MethodSignature signature = (MethodSignature) proceedJp.getSignature();
        //method是代理方法
        Method method = signature.getMethod();
        ResponseAnnotation backAnnotation = method.getAnnotation(ResponseAnnotation.class);

        if (backAnnotation == null) {
            Object obProceed = proceedJp.proceed(); //执行目标方法
            return obProceed;
        }
        return outResponseResult(proceedJp,backAnnotation);
    }

    protected Object outResponseResult( ProceedingJoinPoint pjp, ResponseAnnotation backAnnotation)
            throws Throwable { // NOSONAR
        switch (backAnnotation.type()) {
            case API:
                return responseApi( pjp, backAnnotation);
            default:
                return pjp.proceed();
        }
    }

    protected Object responseApi( ProceedingJoinPoint pjp, ResponseAnnotation backAnnotation)
            throws Throwable { // NOSONAR
        if (null == backAnnotation || StringUtils.isBlank(backAnnotation.name())) {
            throw new IllegalArgumentException("Api Response must set @ResponseAnnotation.");
        }
        String api = backAnnotation.name();
        String version = backAnnotation.apiVersion();

        //下面是根据实际的输出调用
        ApiResponse response = null;
        try {
            Object object = pjp.proceed(); //这个方法会进入到方法里面进行执行,并获得结果到object
             response =  new ApiResponse(api, version, ApiResponse.CODE_OK, ApiResponse.MSG_OK, object);

        } catch (Exception ex) {
       //     log.error("name:"+api+" ,exception:"+ JSON.toJSONString(ex));
            response =  new ApiResponse(api, version,  findSysCode(ex), getMessage(ex), null);
        }
        return response;
    }

    private String findSysCode(Exception ex) {
        if (ex instanceof IOException) {
            return "-4";
        }else {
            return "0";
        }
    }

    private String getMessage(Exception ex) {
        return ex.getMessage();
    }
}
