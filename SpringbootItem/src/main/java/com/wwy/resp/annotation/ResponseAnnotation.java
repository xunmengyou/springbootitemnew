package com.wwy.resp.annotation;


import java.lang.annotation.*;

@Target({ElementType.PARAMETER,ElementType.METHOD})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseAnnotation {

    ResponseType type() default ResponseType.API;
    /**
     * 名称定义  用于api输出的接口名{api}字段输出 ，如{"api":"messageName", "v":"1.1"....}
     * @return
     */
    String name() default "api";
    /**
     * 适用于 type=API。用于输出接口信息的版本号
     * @return
     */
    String apiVersion() default  "0.1";
    /**
     * 事件类型 type=API默认为text/html, type=AJAX默认为application/json
     * @return
     */
    String eventType() default "";
    /**
     * 是否封装返回对象。如果type=AJAX且设置为false，可以自由定义返回数据。
     * @return
     */
    boolean useLog() default true;
    /**
     * JSONP CALLBACK参数名
     * @return
     */
    String callbackParam() default "";
    /**
     * 该请求生效的环境，如果不填，不进行校验，如果填写，则只有对应的profile才可以访问
     * @return
     */
    String profile() default "";
    /**
     * 目标方法的参数意义，与目标方法的参数一起使用，数组长度需与参数长度一致
     * @return
     */
    String[] detailArgs() default {};
}
