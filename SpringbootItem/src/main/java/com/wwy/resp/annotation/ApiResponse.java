package com.wwy.resp.annotation;


import java.io.Serializable;

/**
 * {api:"mts.user.register, v:"1.0", code:"1", msg:"ok", data:{}}
 */
public class ApiResponse implements Serializable {

    public static final String MSG_OK = "";  // 成功MSG值
    public static final String CODE_OK = "1"; //成功RET值

    private String api; //接口名称
    private String v = "1.0"; //版本号
    private String code; //返回码
    private String msg;  //返回描述
    private String etag;  //翻页一致性标签
    private Object data;  //业务数据对象
    private Object ext;
    private Integer realCount;
    public ApiResponse(String api, String v, String code, String msg, Object data) {
        this.v = v;
        this.api = api;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getExt() {
        return ext;
    }

    public void setExt(Object ext) {
        this.ext = ext;
    }

    public Integer getRealCount() {
        return realCount;
    }

    public void setRealCount(Integer realCount) {
        this.realCount = realCount;
    }
}
