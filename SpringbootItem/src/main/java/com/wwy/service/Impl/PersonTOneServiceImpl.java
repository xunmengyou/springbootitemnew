package com.wwy.service.Impl;

import com.wwy.dao.PersonTOneMapper;
import com.wwy.entity.PersonT1;
import com.wwy.service.IPersonTOneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonTOneServiceImpl implements IPersonTOneService {
    @Autowired
    PersonTOneMapper personTOneMapper;

    @Override
    public void insertPerson1(PersonT1 pers1) {
        personTOneMapper.insertPerson1(pers1);
    }

    @Override
    public PersonT1 selectPerson1ById(Integer id) {
        return personTOneMapper.selectPerson1ById(id);
    }

    @Override
    public Integer delPerson1(PersonT1 pers1) {
        return personTOneMapper.delPerson1(pers1);
    }
}
