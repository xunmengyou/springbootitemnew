package com.wwy.service;

import com.wwy.redis.RedisTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.JedisCluster;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

@Service
public class RedisLettuceAndJedisService {

    @Autowired
    RedisTemplateUtil redisTemplateUtil;
//    @Autowired
//    JedisCluster jedisCluster;

    private CountDownLatch latch = new CountDownLatch(20);

    public String stringRedis(){

        redisTemplateUtil.set("name","2222");

        String object=(String)redisTemplateUtil.get("name");
        System.out.println(object);
        return object;
    }


//    public String stringRedisV1(){
//
//        jedisCluster.set("name1","222");
//        String object = jedisCluster.get("name1");
//        try {
//            jedisCluster.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return object;
//    }

    public String multiRedisTemplate(){

        for (int i =0;i<20;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println( Thread.currentThread().getName()+" "+System.currentTimeMillis());
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //不用担心线程池问题，线程池需要调用每个方法后再释放连接
                    redisTemplateUtil.set("name1",Thread.currentThread().getName()+" :"+System.currentTimeMillis());
                    System.out.println( redisTemplateUtil.get("name1"));
                }
            }).start();
            latch.countDown();
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "1";

    }


}
