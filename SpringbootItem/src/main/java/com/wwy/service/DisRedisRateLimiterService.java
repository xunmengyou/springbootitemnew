package com.wwy.service;


import com.wwy.redis.DisRedisRateLimiterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DisRedisRateLimiterService {
    @Autowired
    DisRedisRateLimiterUtil disRedisRateLimiterUtil;

    /**
     * 获取Redis+令牌桶 的令牌
     * @param key
     * @return
     */
    public boolean rateLimiterToken(String key){
        disRedisRateLimiterUtil.initToken(key);//初如化开始
        //true： 没触发限流策略   false: 触发限流
       return disRedisRateLimiterUtil.accquireToken(key,1);
    }
}
