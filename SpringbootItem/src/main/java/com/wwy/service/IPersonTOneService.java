package com.wwy.service;

import com.wwy.entity.PersonT1;

public interface IPersonTOneService {
    public void insertPerson1(PersonT1 pers1);
    public PersonT1 selectPerson1ById(Integer id);
    public Integer delPerson1(PersonT1 pers1);
}
