package com.wwy.service;

import com.wwy.entity.Person;
import com.wwy.service.IPersonService;
import com.wwy.service.IPersonTOneService;
import com.wwy.service.Impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionalService2 {

    @Autowired
    PersonServiceImpl personService;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void test2(int id){

        System.out.println("test2...run");
        Person person = new  Person();
        person.setPersonID(id);
        person.setLastName("q1q");
        person.setFirstName("wq");
        person.setAddress("sdf34w");
        person.setCity("025");
        personService.insertPerson(null,person);
//        PersonT1 pt1 = new PersonT1();
//        pt1.setId(1);
//        personTOneMapper.delPerson1(pt1);
//        int i=1/0;
        System.out.println("test2...finish");
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void test3(int id){

        System.out.println("test3...run");
        Person person = new  Person();
        person.setPersonID(id);
        person.setLastName("test3");
        personService.insertPerson(null,person);
        System.out.println("test3...finish");
        throw new RuntimeException();
    }
}
