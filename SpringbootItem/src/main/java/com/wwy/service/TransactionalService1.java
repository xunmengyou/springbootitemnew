package com.wwy.service;

import com.wwy.entity.PersonT1;
import com.wwy.service.Impl.PersonTOneServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransactionalService1 {

    @Autowired
    PersonTOneServiceImpl personTOneService;

    @Autowired
    TransactionalService2 transactionalService2;

    @Transactional(propagation = Propagation.REQUIRED)
    public void test1(){
        System.out.println("test1...run");
        PersonT1 pt1 = new PersonT1();
        pt1.setName("68");
        pt1.setAge(31);
        personTOneService.insertPerson1(pt1);
        transactionalService2.test2(41);

        transactionalService2.test3(42);
        System.out.println("test1...finish");
    }
}
