/**
 * 
 */
package com.wwy.controller;


import com.wwy.resp.annotation.ResponseAnnotation;
import com.wwy.resp.annotation.ResponseType;
import com.wwy.service.TransactionalService1;
import com.wwy.service.RedisLettuceAndJedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/hello")
public class HelloWorldController {

	@Autowired
	RedisLettuceAndJedisService redisLettuceService;

	@Autowired
	TransactionalService1 transactionalService1;

	@RequestMapping("/transactionTest.do")
	@ResponseAnnotation(type = ResponseType.API,name = "hello-he-01")
	@ResponseBody
	public Object transactionTest(){
		transactionalService1.test1();
		return 1;
	}
	/**
	 * 标准的切面返回，三个注解不能少
	 */
	@RequestMapping("/he.do")
	@ResponseAnnotation(type = ResponseType.API,name = "hello-he-01")
	@ResponseBody
	public Object sayHello() {
	       return "Hello,World!";
	}

	@RequestMapping("/redisTest")
	public Object redisTest(){
		return redisLettuceService.stringRedis();
	}

	@RequestMapping("/redisTest1")
	public Object stringRedisV1(){
		return redisLettuceService.multiRedisTemplate();
	}
}
