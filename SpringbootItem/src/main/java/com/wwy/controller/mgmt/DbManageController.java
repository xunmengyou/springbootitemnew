package com.wwy.controller.mgmt;

import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLDeleteStatement;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.alibaba.druid.sql.ast.statement.SQLUpdateStatement;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlOutputVisitor;
import com.alibaba.druid.sql.parser.ParserException;
import com.alibaba.druid.util.Utils;
import com.alibaba.druid.wall.Violation;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallVisitor;
import com.alibaba.druid.wall.spi.MySqlWallProvider;
import com.alibaba.druid.wall.violation.SyntaxErrorViolation;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * http://localhost:8084/mgmt/dataMgmt.html?sk=mbap20212022mbap
 */
@Controller
@RequestMapping("/admin/mgmt/db/")
public class DbManageController implements InitializingBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(DbManageController.class);

    @Autowired
//    @Qualifier("masterDataSource")
    private DataSource dataSource;

    private MySqlWallProvider sqlWallProvider;

    private JdbcTemplate jdbcTemplate;

    @RequestMapping("select.do")
    public void execute(HttpServletRequest request, HttpServletResponse response) throws HttpMediaTypeNotSupportedException, IOException {

        MediaType mediaType = MediaType.parseMediaType(request.getContentType());
        if (!MediaType.parseMediaType("application/sql").includes(mediaType)) {
            throw new HttpMediaTypeNotSupportedException(mediaType + "not supported");
        }
        Map<String, Object> resMap = new HashMap<>();
        try (Reader reader = new InputStreamReader(request.getInputStream(), mediaType.getCharset())) {//NOSONAR
            final String input = Utils.read(reader);

            try {
                List<Violation> violations = new ArrayList<Violation>();
                SQLStatement stmt = filter(input, violations);
                if (!violations.isEmpty()) {
                    resMap.put("result", "failed");
                    resMap.put("data", JSON.toJSONString(violations));
                    output(response, resMap);
                    return;
                }

                StringBuilder out = new StringBuilder();
                MySqlOutputVisitor visitor = new MySqlOutputVisitor(out);
                stmt.accept(visitor);

                if (stmt instanceof SQLSelectStatement) {
                    resMap.put("result", "success");
                    resMap.put("data", JSON.toJSONString(jdbcTemplate.queryForList(out.toString())));
                    output(response, resMap);
                    return;
                }
                if (stmt instanceof SQLDeleteStatement || stmt instanceof SQLUpdateStatement) {
                    resMap.put("result", "success");
                    resMap.put("affected", JSON.toJSONString(jdbcTemplate.update(out.toString())));
                    output(response, resMap);
                    return;
                }
                jdbcTemplate.execute(out.toString());
                resMap.put("result", "success");
                resMap.put("data", "execute done");
                output(response, resMap);
            }catch (Exception e){//NOSONAR
                LOGGER.error("DbManageController ExecuteError!", e);
                resMap.put("result", "DbManageController ExecuteError!");
                output(response, resMap);
            }
        }
    }

    /**
     * SQL 语句执行网关
     *
     * @return
     * @throws Exception
     */
    private SQLStatement filter(String sql, List<Violation> violations) {
        try {
            MySqlStatementParser parser = new MySqlStatementParser(sql);
            List<SQLStatement> stmtList = parser.parseStatementList();
            if (stmtList.size() != 1) {
                throw new IllegalArgumentException("not support multi-statment");
            }
            SQLStatement stmt = stmtList.get(0);

            WallVisitor wallVisitor = sqlWallProvider.createWallVisitor();
            stmt.accept(wallVisitor);
            if (!wallVisitor.getViolations().isEmpty()) {
                violations.addAll(wallVisitor.getViolations());
                return null;
            } else {
                return stmt;
            }
        } catch (ParserException e) {
            violations.add(new SyntaxErrorViolation(e, sql));
        }
        return null;
    }

    private void output(HttpServletResponse response, Object data){
        response.setContentType("application/json; charset=utf-8");
        try {
            response.getWriter().write(JSON.toJSONString(data));
        } catch (IOException e) { //NOSONAR
            // ignore
        }
    }

    @Override
    public void afterPropertiesSet(){
        WallConfig wallConfig = new WallConfig();
        wallConfig.setMultiStatementAllow(true);
        wallConfig.setDropTableAllow(false);
        wallConfig.setTruncateAllow(false);
        sqlWallProvider = new MySqlWallProvider(wallConfig);

        jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
        // 默认最大200
        jdbcTemplate.setMaxRows(200);
    }
}
