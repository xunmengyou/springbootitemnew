package com.wwy.controller;

import com.wwy.hbase.HBaseOnlineService;
import com.wwy.properties.GetPropertiesClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/starter")
public class TestStarterController {

    //自己创建的starter，然后调用里面的方法
    @Autowired
    GetPropertiesClass getPropertiesClass;

    @Autowired
    HBaseOnlineService hBaseOnlineService;

    @RequestMapping("/sayHello1.do")
    @ResponseBody
    public String sayHello1() {
        String value = getPropertiesClass.get();
        return value;
    }

    @RequestMapping("/hbase.do")
    @ResponseBody
    public Object hbaseOnline() {
        String rowKey = "0209300410000000000070056981010";
        String num = null;
        try {
            num = hBaseOnlineService.getSingleCellValue("ns_mzss:content_noprice_product_data", "cf", "value", rowKey);
        } catch (NumberFormatException e) {
        }
        return num;
    }
}
