package com.wwy.redis;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * https://blog.csdn.net/sinat_22797429/article/details/89196933  其他的方法使用
 */
@Component
public class RedisTemplateUtil {

    @Autowired
    RedisTemplate redisTemplate;

    public Boolean hasKey(String key){
        return redisTemplate.hasKey(key);
    }

    public void set(String key,String value){
        redisTemplate.opsForValue().set(key,value);
    }


    public Object get(String key){
        return redisTemplate.opsForValue().get(key);
    }

    public Boolean expire(String key, long timeOut, TimeUnit unit){
        return redisTemplate.expire(key,timeOut,unit);
    }

}
