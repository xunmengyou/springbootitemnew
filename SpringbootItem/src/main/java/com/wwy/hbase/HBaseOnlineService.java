package com.wwy.hbase;

import org.apache.hadoop.hbase.client.HConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HBaseOnlineService extends HbaseOperatorTableService {

    @Autowired
    HBaseClient hbaseOnlineClient;
//    @Autowired
//    HBaseClient hbaseOfflineClient;
//    //验证了starter可以创建任何对象给外围使用
//    @Autowired
//    String test1;
    @Override
    public HConnection getHbaseClientConnection() {
        if(null != hbaseOnlineClient){
            return hbaseOnlineClient.getConnection(permissionUserName());
        }
        return null;
    }

    @Override
    public String permissionUserName() {
        return "mzss";
    }
}
